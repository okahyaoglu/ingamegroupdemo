﻿using System.Net;
using System.Threading.Tasks;
using InGameGroupDemo.Business;
using InGameGroupDemo.Business.Applications;
using InGameGroupDemo.Data.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace InGameGroupDemo.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController, Authorize]
    public class ProductController : ControllerBase
    {
        [HttpGet,
         ProducesResponseType(typeof(string), (int)HttpStatusCode.OK),
         ProducesResponseType(typeof(ModelStateDictionary), (int)HttpStatusCode.BadRequest),
         Route("detail"), Authorize(Roles = AUTHCONST.ROLES.PRODUCT_VIEW)]
        public async Task<IActionResult> Detail([FromServices]ProductApp app, int productId)
        {
            var entity = await app.FindCachedById(productId);
            if (entity == null)
                return NotFound();
            return Ok(entity);
        }

        [HttpGet,
         ProducesResponseType(typeof(string), (int)HttpStatusCode.OK),
         ProducesResponseType(typeof(ModelStateDictionary), (int)HttpStatusCode.BadRequest),
         Route("list-by-category")]
        public async Task<IActionResult> ListByCategory([FromServices]ProductApp app, int categoryId)
        {
            var entity = await app.FindCachedByCategoryId(categoryId);
            if (entity == null)
                return NotFound();
            return Ok(entity);
        }


        [HttpPost,
         ProducesResponseType(typeof(Category), (int)HttpStatusCode.OK),
         ProducesResponseType(typeof(ModelStateDictionary), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Insert([FromServices]ProductApp app, Product entity)
        {
            var entities = await app.Upsert(entity);
            if (entities == null)
                return NotFound();
            return Ok(entities);
        }

        [HttpPut,
         ProducesResponseType(typeof(Category), (int)HttpStatusCode.OK),
         ProducesResponseType(typeof(ModelStateDictionary), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Update([FromServices]ProductApp app, Product posted)
        {
            var entity = await app.FindCachedById(posted.Id);
            if (entity == null)
                return NotFound();
            await TryUpdateModelAsync(entity);
            var entities = await app.Upsert(entity);
            return Ok(entities);
        }

        [HttpDelete,
         ProducesResponseType(typeof(Category), (int)HttpStatusCode.OK),
         ProducesResponseType(typeof(ModelStateDictionary), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Delete([FromServices]ProductApp app, int id)
        {
            var entity = await app.Delete(id);
            if (entity == 0)
                return NotFound();
            return Ok();
        }

    }
}
