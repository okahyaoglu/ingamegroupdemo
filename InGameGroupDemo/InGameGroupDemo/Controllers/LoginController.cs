﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using InGameGroupDemo.Business;
using InGameGroupDemo.Data.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace InGameGroupDemo.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly ApiMembershipService _membershipService;

        public LoginController(IConfiguration configuration, ApiMembershipService membershipService)
        {
            _configuration = configuration;
            _membershipService = membershipService;
        }

        [HttpPost, AllowAnonymous, ResponseCache(NoStore = true, Duration = 0),
         ProducesResponseType(typeof(string), (int)HttpStatusCode.OK),
         ProducesResponseType(typeof(ModelStateDictionary), (int)HttpStatusCode.BadRequest),
         ProducesResponseType(typeof(ObjectResult), (int)HttpStatusCode.Unauthorized),
         Route("login")]
        public async Task<IActionResult> Login(
            [FromServices] UserManager<AppUser> _userManager,
            [FromForm, Required] string username, [FromForm, Required, MinLength(6)] string password)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var signInResult = await _membershipService.SignIn(username, password);
            if (signInResult.Item1.Succeeded)
                return Ok(await GenerateJwtToken(username, signInResult.Item2, _userManager));
            return BadRequest(ModelState);
        }

        [HttpPost, AllowAnonymous, ResponseCache(NoStore = true, Duration = 0),
         ProducesResponseType(typeof(string), (int)HttpStatusCode.OK),
         ProducesResponseType(typeof(ModelStateDictionary), (int)HttpStatusCode.BadRequest),
         ProducesResponseType(typeof(ObjectResult), (int)HttpStatusCode.Unauthorized),
         Route("forgot-password", Name = "test")]
        public async Task<IActionResult> ForgotPassword([FromForm, Required] string email)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var result = await _membershipService.ForgotPassword(email,
                (userid, code) => Url.Link("forgot-password-change", new { userId = userid, code }));
            return Ok(result);
        }

        [HttpPost, AllowAnonymous, ResponseCache(NoStore = true, Duration = 0),
         ProducesResponseType(typeof(string), (int)HttpStatusCode.OK),
         ProducesResponseType(typeof(ModelStateDictionary), (int)HttpStatusCode.BadRequest),
         ProducesResponseType(typeof(ObjectResult), (int)HttpStatusCode.Unauthorized),
         Route("forgot-password-change", Name = "forgot-password-change")]
        public async Task<IActionResult> ChangePasswordWithToken([FromForm, Required] string token, [FromForm, Required] string email, [FromForm, Required, MinLength(6)] string newPassword)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var result = await _membershipService.ChangePasswordWithToken(email, token, newPassword);
            if (result.Item1 == false)
                return BadRequest(ModelState);
            if (result.Item2.Succeeded == false)
                return BadRequest(result.Item2.Errors);
            return Ok(result);
        }

        //[HttpPost, AllowAnonymous, ResponseCache(NoStore = true, Duration = 0),
        // ProducesResponseType(typeof(string), (int)HttpStatusCode.OK),
        // ProducesResponseType(typeof(ModelStateDictionary), (int)HttpStatusCode.BadRequest),
        // ProducesResponseType(typeof(ObjectResult), (int)HttpStatusCode.Unauthorized),
        // Route("reset-password")
        //]
        //public async Task<IActionResult> ResetPassword([Required] string username)
        //{
        //    if (!ModelState.IsValid)
        //        return BadRequest(ModelState);
        //    var signInResult = await _membershipService.ForgotPassword(username, code =>
        //        Url.Action("ForgotPassword",
        //        null,
        //        new { userId = user.Id, code },
        //        Request.Scheme));
        //    if (signInResult.Item1.Succeeded)
        //        return Ok(GenerateJwtToken(username, signInResult.Item2));
        //    return StatusCode(401);
        //}

        /// <summary>
        /// İlk user'ı oluşturmak için gereken test metotu!
        /// </summary>
        /// <param name="email"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost, AllowAnonymous, ResponseCache(NoStore = true, Duration = 0)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ModelStateDictionary), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ObjectResult), (int)HttpStatusCode.Unauthorized)]
        [Route("create")]
        public async Task<IActionResult> Create(
            [FromServices] UserManager<AppUser> _userManager,
            [FromServices] SignInManager<AppUser> _signInManager,
            [FromServices] RoleManager<IdentityRole> _roleManager,
            [FromForm] bool hasProductViewClaim)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var user = new AppUser
            {
                UserName = "abc@def.com",
                EmailConfirmed = true,
            };
            user.Email = user.UserName;
            var result = await _userManager.CreateAsync(user, "Aa123456");
            if (result.Succeeded)
            {
                if (hasProductViewClaim)
                {
                    if ((await _roleManager.RoleExistsAsync(AUTHCONST.ROLES.PRODUCT_VIEW)) == false)
                        await _roleManager.CreateAsync(new IdentityRole(AUTHCONST.ROLES.PRODUCT_VIEW));
                    await _userManager.AddToRoleAsync(user, AUTHCONST.ROLES.PRODUCT_VIEW);
                }

                await _signInManager.SignInAsync(user, false);
                return Ok(await GenerateJwtToken(user.UserName, user, _userManager));
            }

            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }
            return BadRequest(ModelState);
        }


        private async Task<string> GenerateJwtToken(string email, AppUser user, UserManager<AppUser> _userManager)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddSeconds(Convert.ToDouble(_configuration["JwtExpire"]));

            var roles = await _userManager.GetRolesAsync(user);
            claims.Add(new Claim(ClaimTypes.Role, string.Join(",", roles)));

            var token = new JwtSecurityToken(
                _configuration["JwtIssuer"],
                _configuration["JwtIssuer"],
                claims,
                expires: expires,
                signingCredentials: creds
            );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
