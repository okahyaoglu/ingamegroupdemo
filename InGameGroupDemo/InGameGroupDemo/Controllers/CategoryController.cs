﻿using System.Net;
using System.Threading.Tasks;
using InGameGroupDemo.Business.Applications;
using InGameGroupDemo.Data.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace InGameGroupDemo.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController, Authorize]
    public class CategoryController : ControllerBase
    {
        [HttpGet,
         ProducesResponseType(typeof(string), (int)HttpStatusCode.OK),
         ProducesResponseType(typeof(ModelStateDictionary), (int)HttpStatusCode.BadRequest),
         Route("detail")]
        public async Task<IActionResult> Detail([FromServices]CategoryApp app, int productId)
        {
            var entity = await app.FindCachedById(productId);
            if (entity == null)
                return NotFound();
            return Ok(entity);
        }

        [HttpGet, Route("list"),
         ProducesResponseType(typeof(Category), (int)HttpStatusCode.OK),
         ProducesResponseType(typeof(ModelStateDictionary), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> List([FromServices]CategoryApp app, int? parentCategoryId)
        {
            var entities = await app.GetAllCached(parentCategoryId);
            if (entities == null)
                return NotFound();
            return Ok(entities);
        }

        [HttpPost, 
         ProducesResponseType(typeof(Category), (int)HttpStatusCode.OK),
         ProducesResponseType(typeof(ModelStateDictionary), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Insert([FromServices]CategoryApp app, Category entity)
        {
            var entities = await app.Upsert(entity);
            if (entities == null)
                return NotFound();
            return Ok(entities);
        }

        [HttpPut, 
         ProducesResponseType(typeof(Category), (int)HttpStatusCode.OK),
         ProducesResponseType(typeof(ModelStateDictionary), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Update([FromServices]CategoryApp app, Category posted)
        {
            var entity = await app.FindCachedById(posted.Id);
            if (entity == null)
                return NotFound();
            await TryUpdateModelAsync(entity);
            var entities = await app.Upsert(entity);
            return Ok(entities);
        }

        [HttpDelete,
         ProducesResponseType(typeof(Category), (int)HttpStatusCode.OK),
         ProducesResponseType(typeof(ModelStateDictionary), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Delete([FromServices]CategoryApp app, int id)
        {
            var entity = await app.Delete(id);
            if (entity == 0)
                return NotFound();
            return Ok();
        }
    }
}
