﻿using System.Linq;
using System.Threading.Tasks;
using InGameGroupDemo.Data;
using InGameGroupDemo.Data.Models;
using LazyCache;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;

namespace InGameGroupDemo.Business.Applications
{
    public class CategoryApp
    {
        private readonly CachingService _LazyCache;
        private readonly ingamegroupdemoContext _context;

        public CategoryApp(CachingService lazyCache, ingamegroupdemoContext context)
        {
            _LazyCache = lazyCache;
            _context = context;
        }

        public Task<Category> FindCachedById(int id)
        {
            var result = _LazyCache.GetOrAddAsync($"Category-{id}", () => _context.Category.FindAsync(id));
            return result;
        }

        public async Task<Category[]> GetAllCached(int? parentCategoryID = null)
        {
            var result = await _LazyCache.GetOrAddAsync($"Categories", () => _context.Category.ToArrayAsync());
            if (parentCategoryID != null)
                result = result.Where(c => c.FkParentId == parentCategoryID).ToArray();
            return result;
        }

        public async Task<Category> Upsert(Category entity)
        {
            if (entity.Id == 0)
                _context.Category.Add(entity);
            else
            {
                _context.Category.Attach(entity);
                _context.Entry(entity).State = EntityState.Modified;
            }
            await _context.SaveChangesAsync();
            _LazyCache.Add($"Category-{entity.Id}", entity);
            return entity;
        }

        public Task<int> Delete(int id)
        {
            _LazyCache.Remove($"Category-{id}");
            return _context.Category.Where(c => c.Id == id).DeleteAsync();
        }
    }
}
