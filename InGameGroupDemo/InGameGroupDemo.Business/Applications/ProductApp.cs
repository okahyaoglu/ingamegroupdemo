﻿using System;
using System.Linq;
using System.Threading.Tasks;
using InGameGroupDemo.Data;
using InGameGroupDemo.Data.Models;
using LazyCache;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;

namespace InGameGroupDemo.Business.Applications
{
    public class ProductApp
    {
        private readonly CachingService _LazyCache;
        private readonly ingamegroupdemoContext _context;

        public ProductApp(CachingService lazyCache, ingamegroupdemoContext context)
        {
            _LazyCache = lazyCache;
            _context = context;
        }

        public Task<Product> FindCachedById(int id)
        {
            var result = _LazyCache.GetOrAddAsync($"Product-{id}", () => _context.Product.FindAsync(id));
            return result;
        }

        public Task<Product[]> FindCachedByCategoryId(int categoryId)
        {
            var result = _LazyCache.GetOrAddAsync($"Product-By-Category-{categoryId}", () => _context.Product.Where(p=> p.FkCategoryId == categoryId).ToArrayAsync());
            return result;
        }


        public async Task<Product> Upsert(Product entity)
        {
            if (entity.Id == 0)
                _context.Product.Add(entity);
            else
            {
                _context.Product.Attach(entity);
                _context.Entry(entity).State = EntityState.Modified;
            }
            await _context.SaveChangesAsync();
            _LazyCache.Add($"Product-{entity.Id}", entity);
            return entity;
        }

        public Task<int> Delete(int id)
        {
            _LazyCache.Remove($"Product-{id}");
            return _context.Product.Where(c => c.Id == id).DeleteAsync();
        }

    }
}
