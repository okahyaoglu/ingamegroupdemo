﻿using System;
using System.Security.Policy;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using InGameGroupDemo.Data;
using InGameGroupDemo.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace InGameGroupDemo.Business
{
    public class ApiMembershipService
    {
        private readonly ingamegroupdemoContext _context;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly EmailSender _emailSender;

        public ApiMembershipService(ingamegroupdemoContext context, SignInManager<AppUser> signInManager, EmailSender emailSender)
        {
            _context = context;
            _signInManager = signInManager;
            _emailSender = emailSender;
        }

        public async Task<(SignInResult, AppUser)> SignIn(string username, string password)
        {
            var user = await _context.Users.FirstOrDefaultAsync(e => e.UserName == username);
            if (user == null)
                return (SignInResult.Failed, null);

            //var claims = await _userManager.GetClaimsAsync(user);
            var result = await _signInManager.PasswordSignInAsync(username, password, false, false);
            return (result, user);
        }

        public async Task<bool> ForgotPassword(string email, Func<string, string, string> generateResetPasswordLinkFunc)
        {
            var user = await _context.Users.FirstOrDefaultAsync(e => e.Email == email);
            if (user == null)
                return false;

            var code = await _signInManager.UserManager.GenerateEmailConfirmationTokenAsync(user);
            var callbackUrl = generateResetPasswordLinkFunc(user.Id, code);
            await _emailSender.SendEmailAsync(user.Email, "Confirm your email", $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");
            return true;
        }

        public async Task<(bool, IdentityResult)> ChangePasswordWithToken(string email, string token, string newPassword)
        {
            var user = await _context.Users.FirstOrDefaultAsync(e => e.Email == email);
            if (user == null)
                return (false, null);

            var result = await _signInManager.UserManager.ResetPasswordAsync(user, token, newPassword);
            return (true, result);
        }
    }
}
