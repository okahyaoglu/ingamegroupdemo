﻿using System;
using InGameGroupDemo.Business.Applications;
using InGameGroupDemo.Data;
using InGameGroupDemo.Data.Models;
using LazyCache;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace InGameGroupDemo.Business
{
    public static class BusinessExtensions
    {
        public static void AddBusinessLayer(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<EmailSender>();
            services.Configure<AuthMessageSenderOptions>(opt=>
            {
                opt.SendGridKey = configuration["SendGridKey"];
                opt.SendGridUser = configuration["SendGridUser"];
            });
            services.AddApiAuthentication();
            services.AddSingleton<CachingService>();
            services.AddScoped<ProductApp>();
            services.AddScoped<CategoryApp>();
        }

        private static void AddApiAuthentication(this IServiceCollection services)
        {
            services.AddScoped(typeof(SignInManager<AppUser>));
            services.AddScoped<ApiMembershipService>();
            services.AddIdentity<AppUser,IdentityRole>()
                .AddSignInManager()
                .AddRoleManager<RoleManager<IdentityRole>>()                         
                .AddEntityFrameworkStores<ingamegroupdemoContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                options.Password = new PasswordOptions
                {
                    RequireDigit = true,
                    RequireLowercase = true,
                    RequireNonAlphanumeric = false,
                    RequireUppercase = true,
                    RequiredLength = 8,
                    RequiredUniqueChars = 1
                };
                options.User = new UserOptions
                {
                    RequireUniqueEmail = true,
                    AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@"
                };
                options.Lockout = new LockoutOptions
                {
                    DefaultLockoutTimeSpan = TimeSpan.FromDays(1),
                    MaxFailedAccessAttempts = 3,
                    AllowedForNewUsers = true
                };
            });

            services.ConfigureApplicationCookie(options =>
            {
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromHours(6);
                options.LoginPath = "/auth/sign-in";
                options.AccessDeniedPath = "/error/403";
                options.LogoutPath = "/auth/sign-out";
                options.SlidingExpiration = true;
            });

            /* API Policy */
            services.AddAuthorization(options =>
            {
                options.AddPolicy("API", builder => builder.RequireAuthenticatedUser()
                    .AddAuthenticationSchemes("Bearer"));
            });
        }

    }
}
