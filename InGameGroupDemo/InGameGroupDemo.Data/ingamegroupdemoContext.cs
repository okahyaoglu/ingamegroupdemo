﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Metadata;
using InGameGroupDemo.Data.Models;

namespace InGameGroupDemo.Data
{
    public partial class ingamegroupdemoContext : IdentityDbContext<AppUser>
    {
        public ingamegroupdemoContext()
        {
        }

        public ingamegroupdemoContext(DbContextOptions<ingamegroupdemoContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public IQueryable<fnProductListWithCategory> fnProductListWithCategory(int id) => 
            Query<fnProductListWithCategory>().FromSql($"select * from fnProductListWithCategory({id})");

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //client evaluation kapatıldı: https://forums.devart.com/viewtopic.php?t=36080
            optionsBuilder.ConfigureWarnings(w => w.Throw(RelationalEventId.QueryClientEvaluationWarning));
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Category>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FkParentId).HasColumnName("FK_ParentID");

                entity.Property(e => e.Title).HasMaxLength(500);

                entity.HasOne(d => d.FkParent)
                    .WithMany(p => p.InverseFkParent)
                    .HasForeignKey(d => d.FkParentId)
                    .HasConstraintName("Category_ParentCategory");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FkCategoryId).HasColumnName("FK_CategoryID");

                entity.Property(e => e.Price).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.Title).HasMaxLength(500);
                entity.HasQueryFilter(p => p.IsActive);
            });

            modelBuilder.Entity<AppUser>(e =>
            {
                e.HasKey(q => q.Id);
            });
            modelBuilder.Query<fnProductListWithCategory>();

        }
    }
}
