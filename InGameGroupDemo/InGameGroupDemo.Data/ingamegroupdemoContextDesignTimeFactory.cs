﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace InGameGroupDemo.Data
{
    /// <summary>
    /// Migration işlemleri için DbContext'in connection'ları nereden okuyabileceğini belirtmek için bu class eklendi.
    /// </summary>
    public class ingamegroupdemoContextDesignTimeFactory : IDesignTimeDbContextFactory<ingamegroupdemoContext>
    {
        public ingamegroupdemoContext CreateDbContext(string[] args)
        {
            var root = Directory.GetParent(Directory.GetCurrentDirectory()).FullName;
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Path.Combine(root, "InGameGroupDemo"))
                .AddJsonFile("appsettings.json")
                .Build();
            string connectionString = configuration.GetConnectionString("ingamegroupdemo");
            var builder = new DbContextOptionsBuilder<ingamegroupdemoContext>();
            builder.EnableSensitiveDataLogging(true);
            builder.UseSqlServer(connectionString);
            return new ingamegroupdemoContext(builder.Options);
        }
    }
}
