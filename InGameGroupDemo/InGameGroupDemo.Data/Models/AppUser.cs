﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace InGameGroupDemo.Data.Models
{
    public partial class AppUser: IdentityUser
    {

        [NotMapped]
        public bool IsLockedout => this.LockoutEnd > DateTimeOffset.Now;
    }
}
