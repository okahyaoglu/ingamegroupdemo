﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace InGameGroupDemo.Data.Models
{
    public partial class fnProductListWithCategory
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int? FkCategoryId { get; set; }
        public string ImageUrl { get; set; }
        public decimal? Price { get; set; }
        public bool IsActive { get; set; }
        public string Description { get; set; }
        public string CategoryName { get; set; }
    }
}
