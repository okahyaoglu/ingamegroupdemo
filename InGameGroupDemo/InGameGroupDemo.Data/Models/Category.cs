﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace InGameGroupDemo.Data.Models
{
    public partial class Category
    {
        public Category()
        {
            InverseFkParent = new HashSet<Category>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        
        public int? FkParentId { get; set; }

        [JsonIgnore]
        public Category FkParent { get; set; }
        
        [JsonIgnore]
        public ICollection<Category> InverseFkParent { get; set; }
    }
}
