﻿using System;
using System.Collections.Generic;

namespace InGameGroupDemo.Data.Models
{
    public partial class Product
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int? FkCategoryId { get; set; }
        public string ImageUrl { get; set; }
        public decimal? Price { get; set; }
        public bool IsActive { get; set; }
        public string Description { get; set; }
    }
}
