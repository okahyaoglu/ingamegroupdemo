using System;
using System.Threading.Tasks;
using InGameGroupDemo.Business.Applications;
using InGameGroupDemo.Data;
using InGameGroupDemo.Data.Models;
using LazyCache;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace InGameGroupDemo.Tests
{
    public class CategoryTests
    {
        [Fact]
        public async Task CheckRecursiveCategories()
        {
            var options = new DbContextOptionsBuilder<ingamegroupdemoContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
            using (var context = new ingamegroupdemoContext(options))
            {
                var app = new CategoryApp(new CachingService(), context);
                var root = new Category { Title = "Root", FkParentId = null };
                await app.Upsert(root);

                var category1 = new Category { Title = "Category 1", FkParentId = root.Id };
                await app.Upsert(category1);
                
                var category2 = new Category { Title = "Category 2", FkParentId = root.Id };
                await app.Upsert(category2);

                var childCategories = await app.GetAllCached(root.Id);
                Assert.True(childCategories.Length == 2);
            }
        }
    }
}
