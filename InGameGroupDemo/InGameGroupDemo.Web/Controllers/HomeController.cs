﻿using Microsoft.AspNetCore.Mvc;

namespace InGameGroupDemo.Web.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index(int? categoryid = 1)
        {
            ViewBag.ParentCategoryID = categoryid.Value;
            return View();
        }
    }
}
