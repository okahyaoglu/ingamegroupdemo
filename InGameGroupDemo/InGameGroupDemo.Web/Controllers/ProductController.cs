﻿using Microsoft.AspNetCore.Mvc;

namespace InGameGroupDemo.Web.Controllers
{
    [Route("product")]
    public class ProductController : Controller
    {
        public IActionResult Index(int? categoryId = 1)
        {
            ViewBag.categoryId = categoryId.Value;
            return View();
        }

        [Route("{name}-{id:int}")]
        public IActionResult Detail(int id)
        {
            ViewBag.ProductId = id;
            return View();
        }
    }
}
