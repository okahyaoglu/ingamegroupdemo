﻿using Microsoft.AspNetCore.Mvc;

namespace InGameGroupDemo.Web.Controllers
{
    public class LoginController : Controller
    {
        public IActionResult Index() => View();
        public IActionResult Forgot() => View();
        public IActionResult ForgotPasswordChange(string code) => View(code);
    }
}
