﻿var authToken = {
    get: function () { return sessionStorage.getItem("bearertoken"); },
    set: function (token) { return sessionStorage.setItem("bearertoken", token); }
};

function tableDataLoader(url, data, templateId, doneCallback) {
    $.ajax({
        url: config.apiUrl + url,
        type: 'GET',
        data: data,
        dataType: 'json',
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + authToken.get());
        }
    }).done(function (data) {
        var newRows = $.tmpl(templateId, data);
        if (doneCallback)
            doneCallback(data,newRows);
        else
            newRows.appendTo('table tbody');
    }).fail(function (xhr, error) {
        if (xhr.status == 401)
            location.href = '/login';
        console.log(arguments);
    });
}


function convertToSlug(Text) {
    return Text
        .toLowerCase()
        .replace(/ /g, '-')
        .replace(/[^\w-]+/g, '')
        ;
}