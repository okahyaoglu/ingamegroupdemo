﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace InGameGroupDemo.Web.TagHelpers
{
    /// <summary>
    /// visible attr'une verilen koşul false olduğu durumda tag'i işlemeyi durdurarak gizler, önyüzde çıkmamasını sağlar.
    /// </summary>
    [HtmlTargetElement(Attributes = "visible")]
    public class VisibleTagHelper : Microsoft.AspNetCore.Razor.TagHelpers.TagHelper
    {
        [HtmlAttributeName("visible")]
        public bool Visible { get; set; }

        public override int Order { get; } = +1000000;

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            if (!Visible)
            {
                output.SuppressOutput();
            }
            else
            {
                base.Process(context, output);
            }
        }
    }
}
