﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace InGameGroupDemo.Web.TagHelpers
{
    /// <summary>
    /// conditional-content attribute'u aracılığı ile, verilen content boş olduğu takdirde tag'i komple gizler, dolu ise de tag'in içerisinde html olarak kendisi yazar
    /// </summary>
    [HtmlTargetElement(Attributes = "conditional-content")]
    public class ConditionalContentTagHelper : Microsoft.AspNetCore.Razor.TagHelpers.TagHelper
    {
        [HtmlAttributeName("conditional-content")]
        public string Content { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            if (string.IsNullOrEmpty(Content))
            {
                output.SuppressOutput();
            }
            else
            {
                output.Content.AppendHtml(Content);
                base.Process(context, output);
            }
        }
    }
}
